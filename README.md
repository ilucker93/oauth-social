https://code.tutsplus.com/ru/tutorials/twitter-sign-in-for-rails-application--cms-28097

source code 


#Dotenv  mini-guide

Для этого нужно использовать гем. Откройте Gemfile еще раз и добавьте гем. Добавьте его в свой Gemfile так:


```
#Gemfile
group :development, :test do
  gem 'dotenv-rails'
```
Чтобы установить гем, запустите.
```
undle install
```

В каталоге вашего приложения создайте файл с именем .env.

Откройте его и добавьте свои ключи и токены:

```
#.env
TWITTER_KEY=xxxxxxxxxxxxxx
TWITTER_SECRET=xxxxxxxxxxxxxx
```
Откройте .gitignore и добавьте только что созданный файл.


```
#.gitignore 
.env
```